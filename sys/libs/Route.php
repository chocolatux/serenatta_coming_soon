<?php
class Route {
    const ROUTE_KEY= '__route__';
    const HTTP_GET = 'GET';
    const HTTP_POST = 'POST';
    const HTTP_PUT = 'PUT';
    const HTTP_DELETE = 'DELETE';
    
    private static $_instance;
    private $_routes = array();
    private $_regexes= array();
    private $_route = null;    
    
    public static function getInstance(){
        if(self::$_instance)
            return self::$_instance;

        self::$_instance = new Route();
        return self::$_instance;
    }

    private function _addRoute($method, $route, $controller, $action, $security){
        $this->_routes[] = array('httpMethod' => $method, 'path' => $route, 'controller' => $controller, 'action' => $action, 'security' => $security);
        $this->_regexes[]= "#^{$route}\$#";
    }

    private function _getRoute($route = false, $httpMethod = null){        
        $this->_route = $route ? $route : 
            (isset($_GET[self::ROUTE_KEY]) ? $_GET[self::ROUTE_KEY] : '/');

        if($httpMethod === null)
            $httpMethod = $_SERVER['REQUEST_METHOD'];
        
        foreach($this->_regexes as $ind => $regex){
                
            if(preg_match($regex, $this->_route)){                
                $def = $this->_routes[$ind];
                if($httpMethod != $def['httpMethod']){
                    continue;
                }else{
                    $request = $_REQUEST;
                    $files = $_FILES;
                    $input = file_get_contents('php://input');
                    parse_str($input, $put);

                    if(isset($request[self::ROUTE_KEY])){
                        unset($request[self::ROUTE_KEY]);
                    }
                    return array('controller' => $def['controller'], 'action' => $def['action'], 'security' => $def['security'], 'request' => $request, 'put' => $put, 'files' => $files);
                }
            }
        }
        trigger_error("Could not find route {$this->_route} from {$_SERVER['REQUEST_URI']}", E_USER_NOTICE);
        return array('controller' => 'error', 'action' => 'index', 'request' => array(), 'put' => array(), 'files' => array(), 'security' => false);
    }
    
    private function _formatMethod($method){
        switch($method){
            case 'get':
                return self::HTTP_GET;
            case 'post':
                return self::HTTP_POST;
            case 'put':
                return self::HTTP_PUT;
            case 'delete':
                return self::HTTP_DELETE;
            default:
                return false;
        }
    }

    public function load($file){
        if(!file_exists($file)){
            trigger_error("Routes config file not found", E_USER_NOTICE);
            exit;
        }
        
        $parsed = parse_ini_file($file, true);

        foreach($parsed as $route){
            $method = $this->_formatMethod(strtolower($route['method']));
            if($method){
                $path = $route['path'] ? $route['path'] : '/';
                $controller = $route['controller'] ? $route['controller'] : 'index';
                $action = $route['action'] ? $route['action'] : 'index';

                switch($route['security'])
                {
                    case 'disabled':
                        $security = false;
                        break;
                    default:
                        $security = strstr($route['security'], ':') ? explode(':', $route['security']) : true;
                        break;
                }

                $this->_addRoute($method, $path, $controller, $action, $security);
            }
        }
    }
    
    public function run($route = false, $httpMethod = null){
        $config = Config::getInstance();
        if($route === false){
            $route = isset($_GET[self::ROUTE_KEY]) ? $_GET[self::ROUTE_KEY] : '/';
        }
        
        if($httpMethod === null){
            $httpMethod = $_SERVER['REQUEST_METHOD'];
        }
        
        $routeDef = $this->_getRoute($route, $httpMethod);

        if(! empty($routeDef['controller'])){
            $controllerName = strtolower($routeDef['controller']);
            $controllerName[0] = strtoupper($controllerName[0]);
            $controllerName = $controllerName . 'Controller';
        }else{
            $controllerName = "IndexController";
        }
        
        if(! empty($routeDef['action']))
            $actionName = $routeDef['action'];
        else
            $actionName = "index";

        $request = $routeDef['request'];
        $files = $routeDef['files'];
        $put = $routeDef['put'];
        $controllerPath = $config->get('controllersFolder') . $controllerName . '.php';
        
        if(is_file($controllerPath)){
            require_once $controllerPath;
        }else{
            die("Controller {$controllerName} - 404 not found");
        }
        
        if(!is_callable(array($controllerName, $actionName))){
            die($controllerName . '->' . $actionName . ' not found');
            return false;
        }

        $controller = new $controllerName();
        $controller->setRequest($request);
        $controller->setPut($put);
        $controller->setFiles($files);
        
        if(method_exists($controller, 'init')){
            $controller->init();
        }

        /*
         * SE COMENTA ESTE BLOQUE POSTERIOR DE SEGURIDAD DE MANERA TEMPORAL
         * YA QUE NO SE ESTAN REQUIRIENDO POR LO PRONTO MANEJAR PERFILES
         * SI SE MANEJAN, ACTIVAR
         */

        //Se valida si la seguridad estará en el controlador o es una credencial
        if($routeDef['security'] === true)
        {
            Seguridad::validarEjecutarAccion($routeDef['controller'], $routeDef['action']);
        }
        elseif($routeDef['security'] !== false)
        {
            Seguridad::validarEjecutarAccion($routeDef['security'][0], $routeDef['security'][1]);
        }
        
        return $controller->$actionName();
    }    
}
