<?php
class View{

    function __construct(){
    }
    
    public function show($name, $vars = array()){
        $config = Config::getInstance();
        $path = $config->get('viewsFolder') . $name;

        if(file_exists($path) == false){
            trigger_error("Template {$path} does not exist.", E_USER_NOTICE);
            return false;
        }
        
        if(is_array($vars)){
            foreach($vars as $key => $value){
                $$key = $value;
            }
        }
        
        require($path);
    }

    public function get($name, $vars = array()){
        $config = Config::getInstance();
        $path = $config->get('viewsFolder') . $name;

        if(file_exists($path) == false){
            trigger_error("Template {$path} does not exist.", E_USER_NOTICE);
            return false;
        }

        if(is_array($vars)){
            foreach($vars as $key => $value){
                $$key = $value;
            }
        }

        ob_start();
        require($path);
        $content = ob_get_clean();

        return $content;
    }

    public function showMain($sectionFile, $vars=array())
    {
        $vars['sSectionFile'] = $sectionFile;
        $this->show('common/main.php', $vars);
    }

    public function showAdminLogin($sectionFile, $vars=array())
    {
        $vars['sSectionFile'] = $sectionFile;
        $this->show('admin/common/login.php', $vars);
    }

    public function showAdminMain($sectionFile, $vars=array())
    {
        $vars['sSectionFile'] = $sectionFile;
        $this->show('admin/common/main.php', $vars);
    }

    public function showJson($data, $vars=array())
    {
        $vars['data'] = $data;
        $this->show('common/json.php', $vars);
    }

    public function showDownload($file, $vars=array())
    {
        $vars['file'] = $file;
        $this->show('common/download.php', $vars);
    }
}