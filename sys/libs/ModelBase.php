<?php
require_once 'DbPDO.php';
require_once $config->get('utilsFolder') . 'ValidarDatos.php';


abstract class ModelBase {
    
    protected $_db;
    protected $_config;
    protected $_lang;
    protected $_error;
    protected $_notice;
    protected $_idUsuario;
    
    protected $sTable = '';
    protected $sPrimaryKey = 'ID';
    
    const CREATED_AT = 'TS';
    const UPDATED_AT = 'UTS';
    const CREATED_BY = 'UID';
    const UPDATED_BY = 'UUID';
    
    protected $aColumns = array();

    public function __construct($aColumns = array())
    {
        $this->_config = Config::getInstance();
        $this->_error = array();
        $lang = Session::get('lang') ? Session::get('lang') : $this->_config->get('defaultLang');
        $this->_idUsuario = Session::get('idUsuario');
         
        $this->_lang = new Translator($lang);
        
        $oConnector = new DbPDO($this->_config->get('dbhost'), 
                $this->_config->get('dbname'), 
                $this->_config->get('dbuser'), 
                $this->_config->get('dbpass')
            );
        
        $this->_db = Db::getInstance($oConnector);
        
        
        $this->getColumnsDb();
        $this->setColumns($aColumns);
    }
    
    public function getLang()
    {
        return $this->_lang;
    }
    
    public function getError()
    {
        return $this->_error;
    }

    public function addError($error)
    {
        $this->_error[] = $error;
    }
    
    public function getNotice()
    {
        return $this->_notice;
    }

    public function addNotice($notice)
    {
        $this->_notice[] = $notice;
    }

    function __get($sAtt)
    {
        if (array_key_exists($sAtt, $this->aColumns)) {
            return $this->aColumns[$sAtt];
        }

        $aTrace = debug_backtrace();
        trigger_error(
            'Atributo indefinido mediante __get(): ' . $sAtt .
            ' en ' . $aTrace[0]['file'] .
            ' en la línea ' . $aTrace[0]['line'],
            E_USER_NOTICE);
        return null;
    }

    function __set($sAtt, $mValue)
    {
        if (array_key_exists($sAtt, $this->aColumns)) {
            $this->aColumns[$sAtt] = $mValue;

            return true;
        }

        $aTrace = debug_backtrace();
        trigger_error(
            'Atributo indefinido mediante __set(): ' . $sAtt .
            ' en ' . $aTrace[0]['file'] .
            ' en la línea ' . $aTrace[0]['line'],
            E_USER_NOTICE);
        return false;
    }

    public function setDataWhere(array $aWhere)
    {
        $aColumns = array('*');
        
        $aDatos = $this->_db->find($this->sTable, $aColumns, $aWhere);
        
        if(!is_array($aDatos) || count($aDatos) == 0) {
            
            return false;
        }
        
        $this->setColumns($aDatos);

        return true;
    }
    
    public function save()
    {
        $sPrimaryKey = $this->sPrimaryKey;
        
        if(ValidarDatos::esId($this->$sPrimaryKey)) {
            
            $this->updateAll();
        } else {
            $this->saveNew();
        }
    }
    
    public function update($aDatos)
    {
        $aColumns = array();
        
        $sPrimaryKey = $this->sPrimaryKey;
        
        foreach($this->aColumns as $sColumn => $sValue)
        {
            if(isset($aDatos[$sColumn]) || array_key_exists($sColumn, $aDatos))
            {
                $aColumns[$sColumn] = $aDatos[$sColumn];
            }
        }

        $this->setTimestamUpdate($aColumns);
        $this->setUserUpdate($aColumns);

        $bResult = $this->_db->update($this->sTable, $aColumns, $this->sPrimaryKey, $this->$sPrimaryKey);
        
        return $bResult;
    }
    
    public function delete()
    {
        $sPrimaryKey = $this->sPrimaryKey;
        
        if(ValidarDatos::esId($this->$sPrimaryKey)) {
            
            $this->_db->delete($this->sTable, $this->sPrimaryKey, $this->$sPrimaryKey);
        } else {
            
            //return false;
            return true;
        }
    }

    public static function deleteAll($fieldName, $fieldValue)
    {
        $oModel = new static();

        $oModel->_db->delete($oModel->sTable, $fieldName, $fieldValue);

        return $oModel;
    }

    public static function create(array $aDatos)
    {
        $oModel = new static($aDatos);

        $oModel->save();

        return $oModel;
    }
    
    public static function find(array $aWhere)
    {
        $oModel = new static();
        
        $bResult = $oModel->setDataWhere($aWhere);
        
        if( !$bResult ) {
            
            return false;
        }

        return $oModel;
    }
    
    public static function findById($nID)
    {
        $oModel = new static();
        
        $aWhere = ["{$oModel->sPrimaryKey} = {$nID}"];
        $mModel = self::find($aWhere);
        
        if( !$mModel ) {
            
            return false;
        }

        return $mModel;
    }
    
    public static function getAll($sQuery)
    {
        $oModel = new static();
        
        $aDatos = $oModel->_db->getAll($sQuery);

        return $aDatos;
    }

    public static function where(array $aWhere, array $aColumns = array('*'))
    {
        $oModel = new static();

        $aDatos = $oModel->_db->findAll($oModel->sTable, $aColumns, $aWhere);

        return $aDatos;
    }

    public function setColumns($aDatos)
    {
        if(is_array($aDatos) && count($aDatos) > 0) {

            foreach($this->aColumns as $sColumn => $sValue) {
                
                if(isset($aDatos[$sColumn]) || array_key_exists($sColumn, $aDatos)) {
                    
                    $this->aColumns[$sColumn] = $aDatos[$sColumn];
                }
            }
        }
    }

    public function getData()
    {
        if(!$this->aColumns)
            return array();

        return (object) $this->aColumns;
    }

    public static function beginTransaction()
    {
        $oModel = new static();
        $oModel->_db->execute('BEGIN');
    }

    public static function rollbackTransaction()
    {
        $oModel = new static();
        $oModel->_db->execute('ROLLBACK');
    }

    public static function commitTransaction()
    {
        $oModel = new static();
        $oModel->_db->execute('COMMIT');
    }

    private function getColumnsDb()
    {
        $sQuery = "DESCRIBE {$this->sTable}";
        
        $aInfoTabla = $this->_db->getAll($sQuery);
        
        foreach ($aInfoTabla as $aUnaColumna) {
            
            $this->aColumns[$aUnaColumna['Field']] = NULL;
        }
    }
    
    private function setTimestampCreate(&$aColumns = NULL)
    {
        $sColumn = self::CREATED_AT;
        
        if($aColumns == NULL) {
            $this->$sColumn = date('Y-m-d H:i:s');
        }else{
            $aColumns[$sColumn] = date('Y-m-d H:i:s');
        }
        
    }
    
    private function setTimestamUpdate(&$aColumns = NULL)
    {
        $sColumn = self::UPDATED_AT;
        
        if($aColumns == NULL) {
            $this->$sColumn = date('Y-m-d H:i:s');
        } else {
            $aColumns[$sColumn] = date('Y-m-d H:i:s');
        }
    }
    
    private function setUserCreate(&$aColumns = NULL)
    {
        $sColumn = self::CREATED_BY;
        
        if($aColumns == NULL) {
            $this->$sColumn = $this->_idUsuario;
        } else {
            $aColumns[$sColumn] = $this->_idUsuario;
        }
    }
    
    private function setUserUpdate(&$aColumns = NULL)
    {
        $sColumn = self::UPDATED_BY;
        
        if($aColumns == NULL) {
            $this->$sColumn = $this->_idUsuario;
        } else {
            $aColumns[$sColumn] = $this->_idUsuario;
        }
        
        
    }
    
    private function saveNew()
    {
        $this->setTimestampCreate();
        $this->setTimestamUpdate();
        $this->setUserCreate();
        $this->setUserUpdate();
        $this->ON = 1;
        
        $bResult = $this->_db->insert($this->sTable, $this->aColumns);
        
        if($bResult) {
            $sPrimaryKey = $this->sPrimaryKey;
            $this->$sPrimaryKey = $bResult;
        }
        
        return $bResult;
    }
    
    private function updateAll()
    {
        $sPrimaryKey = $this->sPrimaryKey;
        
        $this->setTimestamUpdate();
        $this->setUserUpdate();
        
        $bResult = $this->_db->update($this->sTable, $this->aColumns, $this->sPrimaryKey, $this->$sPrimaryKey);
        
        return $bResult;
    }
}
