<?php 
/** 
 * Clase utilizada para lanzar excepciones con fin de bitacorizar cada excepcion lanzada
 * 
 * @author Luis Alberto Garcia 13/04/2013
 */

class Excepcion extends Exception {

  /**
   * 
   * @param string $sModulo
   * @param string $sMensaje
   * @param string $sCodigo
   * @param string $sPrevious
   * 
   * @author Luis Alberto Garcia 13/04/2013
   */
  public function __construct ($sModulo, $sMensaje, $sCodigo, $sPrevious = null)
  {
    //Se crea el registro del evento
    $oLog = new Log();
    
    parent::__construct($sMensaje, 1, $sPrevious);
  }
}
