<?php
/**
 * Clase Correo
 * La clase contiene fnciones utilizadas para la validación de permisos
 * o datos recibidos
 * 
 * Creado 12/Abril/2015
 * 
 * @category Class
 * @package Middleware
 * @author David Heredia <davidchocolatux.com>
 */
require_once $config->get('libsFolder').'email_attachment.php';

class Correos {
    
    /**
     * Objeto Config
     * 
     * @var Config 
     */
    public $_config;
    
    /**
     *
     * @var SysTemplatesCorreos
     */
    var $_oTemplate = null;
    
    /**
     * 
     */
    var $_oBitacoraCorreos = null;
    
    /**
     * Contructor de la clase
     */
    function __construct()
    {
        $this->_config = Config::getInstance();
    }
    
    /**
     * Realiza en envio de un correo registrando el envio en caso de ser
     * exitoso en la bitacora de correos
     * 
     * @param array $aTo
     * @param string $sFrom
     * @param string $sSubject
     * @param string $sTipo
     * @param array $sFile Nombre del archivo
     * @return bool
     */
    public function enviarMail($aTo, $sFrom, $sSubject, $sFile, $aParams)
    {
        $sTemplate = $this->getTemplateContent('main.php');
        $sContent = $this->getTemplateContent($sFile, $aParams);

        $sTemplate = str_replace('@@@CONTENT@@@', $sContent, $sTemplate);

        if($sTemplate)
        {
            // Create new instance of email
            $email = new Email();

            //Se configura el correo
            $email->set_sender($sFrom);

            //Se agregan los recipientes
            foreach($aTo as $sTo)
            {
                $email->add_recipient($sTo);
            }

            $email->set_subject($sSubject);
            $email->set_content($sTemplate);

            //die($email->CONTENT_HTML);

            //Se envía el correo
            $bResult = $email->send();

            return $bResult;
        }

        return false;
    }
    
    /**
     * Regresa el texto del template recibido
     * 
     * @param string $sTipo
     * @return string|bool
     */
    private function getTemplateContent($sFile, $aParams)
    {
        $template = $this->_config->get('viewsFolder') . 'templatesCorreos/' . $sFile;

        if(!file_exists($template)) {
            return false;
        }

        ob_start();
        require($template);
        $sTemplate = ob_get_contents();
        ob_end_clean();

        return $sTemplate;
    }
}
