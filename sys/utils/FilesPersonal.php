<?php
/**
 * Clases usauda para el manejo de los arcivos propios del personal
 * 
 * Creado 12/Abril/2015
 * 
 * @category Class
 * @package utils\FilesPersonal
 * @author Luis Alberto García Fonseca <luis.garcia@chocolatux.com>
 */
require_once $config->get('utilsFolder').'Files.php';
require_once $config->get('utilsFolder').'Excepcion.php';

class FilesPersonal extends Files {

    /**
     * Carga el archivo al servidor y agrega su registro en BD
     * 
     * @param type $nIdPersonal
     */
    public function descargarIFE($nIdPersonal)
    {
        return parent::descargarArchivo('personal_ife', $nIdPersonal);
    }
    
    /**
     * Carga el archivo al servidor y agrega su registro en BD
     * 
     * @param int $nIdPersonal
     * @return bool
     */
    public function descargarComprobanteDomicilio($nIdPersonal)
    {
        return parent::descargarArchivo('personal_comprobanteDomicilio', $nIdPersonal);
    }
    
    /**
     * Carga el archivo al servidor y agrega su registro en BD
     * 
     * @param type $nIdPersonal
     * @param type $aFile
     * @param type $bSobreEscribir
     */
    public function subirIFE($nIdPersonal, $aFile, $bSobreEscribir = false)
    {
        return parent::subirArchivo('personal_ife', $nIdPersonal, $aFile, $bSobreEscribir);
    }
    
    /**
     * Carga el archivo al servidor y agrega su registro en BD
     * 
     * @param int $nIdPersonal
     * @param array $aFile
     * @param bool $bSobreEscribir
     * @return bool
     */
    public function subirComprobanteDomicilio($nIdPersonal, $aFile, $bSobreEscribir = false)
    {
        return parent::subirArchivo('personal_comprobanteDomicilio', $nIdPersonal, $aFile, $bSobreEscribir);
    }
    
    /**
     * Valida que la IFE sea valida
     * 
     * @param array $aFile
     * @return bool
     */
    public function validarIFE($aFile)
    {
        return parent::validarArchivo('personal_ife', $aFile);
    }
    
    /**
     * Valida que el comprobante de domicilio sea valido
     * 
     * @param array $aFile
     * @return bool
     */
    public function validarComprobanteDomicilio($aFile)
    {
        return parent::validarArchivo('personal_comprobanteDomicilio', $aFile);
    }
    
    /**
     * Carga el archivo al servidor y agrega su registro en BD
     * 
     * @param type $nIdPersonal
     * @param type $aFile
     * @param type $bSobreEscribir
     */
    public function subirFoto($nIdPersonal, $aFile, $bSobreEscribir = false)
    {
        return parent::subirArchivo('personal_foto', $nIdPersonal, $aFile, $bSobreEscribir);
    }
}
