<?php
/**
 * Clase IndexController
 * 
 * La clase manipula las acciones del index
 * 
 * Creado 7/Abril/2017
 * 
 * @category Class
 * @package Controllers
 * @author David Heredia <david@chocolatux.com>
 */
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('middlewareFolder').'Seguridad.php';
require_once $config->get('utilsFolder').'Correos.php';
require_once $config->get('modelsFolder').'modelCorreos/ModelCorreos.php';

class IndexController extends ControllerBase {

    public function init()
    {

    }

    public function goIndex()
    {
        //Item activo del menú
        $data['activeItem'] = 'inicio';

        $this->_view->showMain('inicio.php', $data);
    }

    public function ajaxEnviar()
    {
        $sCorreo = $this->_request['email'];

        if($sCorreo == '')
        {
            $this->_view->showJson(array('success' => false, 'message' => 'Error correo no válido'));
            return;

        }

        if(ModelCorreos::findByCorreo($sCorreo))
        {
            $this->_view->showJson(array('success' => false, 'message' => 'Error el correo ingresado ya existe'));
            return;

        }

        $correos = new Correos();

        $bResult = $correos->enviarMail(array($sCorreo), $this->_config->get('projectEmailFrom'), "Bienvenido a Serenatta", "index.php");

        ModelCorreos::guardarCorreo(array('email' => $sCorreo));

        if($bResult){
            $this->_view->showJson(array('success' => true, 'message' => 'Gracias por unirte a Serenatta, te informaremos cuándo el proyecto esté terminado'));
            return;
        }

    }

}