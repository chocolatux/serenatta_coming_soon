<?php
/**
 * Clase ErrorController
 *
 * La clase controla las funciones cuando ocurren errores
 * en el sistema
 *
 * Creado 7/Abril/2017
 *
 * @category Class
 * @package Controllers
 * @author David Heredia <david@chocolatux.com>
 */
class ErrorController extends ControllerBase{
    public function init()
    {
        
    }

    public function index()
    {
        $sSectionFile = 'error';
        $this->_view->show($sSectionFile);
    }
    
    public function goPermisoDenegado()
    {
        $sSectionFile = 'permisoDenegado.php';

        $this->_view->show($sSectionFile);
    }
}