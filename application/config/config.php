<?php
$config = Config::getInstance();

$config->set('configFolder', './../application/config/');
$config->set('libsFolder', './../sys/libs/');
$config->set('utilsFolder', './../sys/utils/');
$config->set('controllersFolder', './../application/controllers/');
$config->set('middlewareFolder', './../application/middleware/');
$config->set('modelsFolder', './../application/models/');
$config->set('viewsFolder', './../application/views/');
$config->set('imagesFolder', 'images/');
$config->set('langFolder', './../application/lang/');
$config->set('includeFolder', './../include/');
$config->set('filesFolder', $_SERVER['DOCUMENT_ROOT'].'/../files/');

$config->set('defaultLang', 'es');
$config->set('projectName', 'PHDTalks');
$config->set('projectClient', '');
$config->set('projectVersion', '1.0.0');
$config->set('projectLastUpdate', '14/10/2015');
$config->set('projectEmailFrom', 'phdtalks@pelfasoft.com');
$config->set('sessionName', 'phdtalks');


//[Development]
$config->set('baseRedirect', '/');
$config->set('baseUrl', '/');

//[Consts Globals]
$config->set('formatoFechaVista', 'dd/mm/yyyy');
$config->set('formatoFechaVistaDate', 'd/m/Y');
$config->set('formatoFechaNombreMes', 'dd/M/yyyy');

$config->set('maxFilesFolder', '10000');