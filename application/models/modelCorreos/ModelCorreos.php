<?php
/**
 * Created by PhpStorm.
 * User: emmanuel
 * Date: 30/05/17
 * Time: 11:59
 */

class ModelCorreos extends ModelBase
{
    protected $sPrimaryKey = 'ID';
    protected $sTable = 'correos';

    public static function guardarCorreo($aCorreo)
    {
        //Se crea el registro en la BD
        $oCorreo = self::create($aCorreo);

        return $oCorreo;

    }

    public static function findByCorreo($sCorreo)
    {
        if(!$aCorreo = self::where(array("`ON` = '1' AND email =  '{$sCorreo}'"), array('ID', 'email')))
        {
            return false;
        }

        return $aCorreo;

    }

}