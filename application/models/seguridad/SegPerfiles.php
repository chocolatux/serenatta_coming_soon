<?php
/**
 * Modelo de la tabla seg_perfiles
 * 
 * Creado 7/Abril/2017
 * 
 * @category Class
 * @package Models\Seguridad
 * @author David Heredia <david@chocolatux.com>
 */
require_once $config->get('modelsFolder').'traits/TemplateCatalogo.php';

class SegPerfiles extends ModelBase {

    use TemplateCatalogo;
    
    /**
     * Nombre de la tabla del modelo
     * @var string
     */
    protected $sTable = 'seg_perfiles';
    
    /**
     * Atributo utilizado para definir la columna que lleva el
     * nombre del registro para los catalogos,
     * ej. pais, estado, etc..
     * 
     * @var string 
     */
    protected $sColumnNombre = 'perfil';
}
