<?php
/**
 * Modelo de la tabla seg_permisos
 * 
 * Creado 7/Abril/2017
 * 
 * @category Class
 * @package Models\Seguridad
 * @author David Heredia <david@chocolatux.com>
 */

class SegPermisos extends ModelBase {

    /**
     * Nombre de la tabla del modelo
     * @var string
     */
    protected $sTable = 'seg_permisos';
    
    /**
     * Regresa un arreglo con los permisos organizados por modulo
     * 
     * @param int $nIdPerfil
     * @return array
     * {
     *  Modulo => {Pemiso, Permiso2, ...},
     *  Modulo2 => {....}
     * }
     */
    public static function obtenerPermisosPerfil($nIdPerfil)
    {
        $oModel = new static;
        
        $sQuery = "SELECT A.accion, P.descripcion, P.idModulo, M.controller, M.descripcion "
                . "FROM seg_permisos_perfiles as PP "
                . "INNER JOIN seg_permisos as P  ON P.ID = PP.idPermiso "
                . "INNER JOIN sys_modulos as M ON M.ID = P.idModulo "
                . "INNER JOIN sys_accionesModulos as A ON A.ID = P.idAccion "
                . "WHERE PP.idPerfil = {$nIdPerfil}";
        
        $aPermisos = $oModel->_db->getAll($sQuery);
        
        $aPermisosEstruc = array();
        
        foreach($aPermisos as $aUnPermiso) {
            
            $aPermisosEstruc[$aUnPermiso['controller']][] = $aUnPermiso['accion'];
        }
        
        return $aPermisosEstruc;
    }
}
