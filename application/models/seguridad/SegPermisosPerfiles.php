<?php 
/**
 * Modelo de la tabla seg_perfilesPermisos
 * 
 * Creado 7/Abril/2017
 * 
 * @category Class
 * @package Models\Seguridad
 * @author David Heredia <david@chocolatux.com>
 */

class SegPermisosPerfiles extends ModelBase {

    /**
     * Nombre de la tabla del modelo
     * @var string
     */
    protected $sTable = 'seg_perfilesPermisos';
}
