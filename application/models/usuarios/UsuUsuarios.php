<?php
/**
 * Modelo de la tabla usu_usuarios
 * 
 * Creado 7/Abril/2017
 * 
 * @category Class
 * @package Models\Usuarios
 * @author David Heredia <david@chocolatux.com>
 */

class UsuUsuarios extends ModelBase {

    /**
     * Nombre de la tabla del modelo
     * @var string
     */
    protected $sTable = 'usu_usuarios';

    /**
     * Regresa un arreglo con los datos del usuario incluyendo
     * perfil y nombre completo
     * 
     * @param int $nUsuario
     * @return array
     */
    public static function obtenerUsuarioInfo($nUsuario)
    {
        $oModel = new static;

        //Query para obtener la información del usuario
        $sQueryUsuarioPerfil = "
            SELECT
            CONCAT(usu_nombre) nombre,
            usu_usuarios.idPerfil, seg_perfiles.perfil as perfil
            FROM usu_usuarios
            INNER join seg_perfiles ON usu_usuarios.idPerfil = seg_perfiles.ID
            WHERE usu_usuarios.ID = '$nUsuario'
        ";

        return $oModel->_db->getOne($sQueryUsuarioPerfil);
    }

}
