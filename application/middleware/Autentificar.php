<?php
/**
 * Clase Autentificar
 * La clase contiene fnciones utilizadas va ra la validación de diversos tipos
 * de datos
 * 
 * Creado 12/Abril/2015
 * 
 * @category Class
 * @package Middleware
 * @author Luis Alberto García Fonseca <luis.garcia@chocolatux.com>
 */
require_once $config->get('modelsFolder').'usuarios/UsuUsuarios.php';
require_once $config->get('modelsFolder').'seguridad/SegPermisos.php';

class Autentificar {
    
    /**
     * Valida si se tiene una sesión iniciada
     * 
     * @return bool
     */
    public static function tieneLogin()
    {
        if( ! Session::get('idUsuario', false) ) {
            
            return false;
        }
        
        return true;
    }
    
    /**
     * Valida si se tiene una sesión iniciada
     * en caso de no tenerlalo redirige al login
     * 
     * @return bool
     */
    public static function validarLogin()
    {
        if( ! self::tieneLogin() ) {
            
            Utils::redirect(Config::getInstance()->get('baseUrl') . "login");
        }
    }
    
    /**
     * Reliza el login de un usuario
     * 
     * @param string $sUsuario
     * @param string $sPassword
     * @return boolean
     */
    public static function login($sUsuario, $sPassword)
    {
        //Bandera de control del resultado
        $bResult = false;

        /* LUEGO NOS PONDREMOS DE ACUERDO CON ESTA ESTRUCTURA
        $aWhere = array(
            array('usuario', '=', $sUsuario, '1'),
            array('password', '=', Utils::encriptMD5($sPassword), '1', 'AND')
        );*/

        //Filtros para buscar el usuario
        $aWhere = array(
            "`ON` = '1' AND (usuario = '{$sUsuario}' OR ",
            "correo = '{$sUsuario}')",
            "AND password = '" . Utils::encriptMD5($sPassword) . "'"
        );

        //Se busca el usuario
        if(!$oUsuUsuario = UsuUsuarios::find($aWhere))
        {
            //En caso de no encontrar coincidencias
            return $bResult;
        }

        //Si se encuentra una coincidencia se obtiene
        if(($oUsuUsuario->usuario == $sUsuario || $oUsuUsuario->correo == $sUsuario) && $oUsuUsuario->password == Utils::encriptMD5($sPassword))
        {
            //Se obtiene la información del usuario
            $aDatosUsuario = $oUsuUsuario->obtenerUsuarioInfo($oUsuUsuario->ID);
            
            //Se obtienen los permisos del perfil del usuario
            $aPermisos = SegPermisos::obtenerPermisosPerfil($aDatosUsuario['idPerfil']);

            //Se agregan las variables de sesión
            //Session::start();
            Session::add('idUsuario', $oUsuUsuario->ID);
            Session::add('idPersonal', $oUsuUsuario->idPersonal);
            Session::add('idPerfil', $oUsuUsuario->idPerfil);
            Session::add('nombreCompleto', $aDatosUsuario['nombreCompleto']);
            Session::add('perfil', $aDatosUsuario['perfil']);
            Session::add('permisos', $aPermisos);

            $bResult = true;
        }

        return $bResult;
    }
    
    /**
     * 
     */
    public static function logout()
    {
        Session::close();
    }
}
