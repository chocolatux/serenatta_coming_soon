<?php
/**
 * Clase Seguridad
 * La clase contiene funciones utilizadas para la validación de permisos
 * 
 * Creado 12/Abril/2015
 * 
 * @category Class
 * @package Middleware
 * @author Luis Alberto García Fonseca <luis.garcia@chocolatux.com>
 */
require_once $config->get('libsFolder') . 'Session.php';

class Seguridad {
    
    static protected $_namespace = 'permisos';
    
    /**
     * 
     * @param string $sController
     * @param string $sAction
     * @return bool True si tiene permiso o false en caso contrario
     */
    public static function validarAccion($sController, $sAction)
    {
        $bPermiso = false;
        $aPermisos = Session::get(self::$_namespace, array());
        
        //Se valida si la accion esta incluida en el perfil del usuario
        if (isset($aPermisos[$sController]) && in_array($sAction, $aPermisos[$sController])) {
            
            $bPermiso = true;
        }
        
        return $bPermiso;
    }
    
    /**
     * Válida un conjunto de permisos de uno o varios controllers.
     * Para validar de un solo controller se envia el nombre del controller
     * como primer argumento y un arreglo con las acciones a validar.
     * Para validar varias acciones de varios controllers se envia un arreglo
     * teniendo como llave el controller y como valor la accion a validar
     * 
     * @param array $aControllersActions Arreglo con el controller y la accion a validar ['adminitradorUsusarios' => 'goListado']
     * @return bool True si tiene alguno de los permisos o false en caso contrario
     */
    public static function validarAcciones($aControllersActions)
    {   
        $nNumArg = func_num_args();
        if($nNumArg == 1) { 
            foreach ($aControllersActions as $sController => $sAction) {

                if( self::validarAccion($sController, $sAction) ) {

                    return true;
                }
            }
        } elseif($nNumArg == 2) {
            
            $sController = func_get_arg(0);
            $aActions = func_get_arg(1);
            
            foreach ($aActions as $sAction) {

                if( self::validarAccion($sController, $sAction) ) {

                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @param string $sController
     * @param string $sAction
     * @return bool True si tiene permiso o false en caso contrario
     */
    public static function validarEjecutarAccion($sController, $sAction)
    {
        if( !self::validarAccion($sController, $sAction) ) {
            
            Utils::redirect(Config::getInstance()->get('baseUrl') . "permisoDenegado");
        }
    }
}
