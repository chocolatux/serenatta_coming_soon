<div id="footer" class="ui stackable inverted equal height stackable grid middle aligned">
    <div class="four wide column">

    </div>
    <div class="eight wide column center aligned">
        <a class="ui image middle aligned" href="http://www.chocolatux.com" title="Desarrollado por ChocolatUX">
            <img src="<?echo($config->get('baseUrl'))?>assets/img/logoChoco.png">
        </a>
    </div>
    <div class="four wide column center aligned">
        <div class="middle aligned">

            <a href="http://www.facebook.com/serenattaoficial">
                <img id="faceLogo" src="<?echo($config->get('baseUrl'))?>assets/img/faceLogo.png">
            </a>

            <a href="http://www.twitter.com/chocolatux">
                <img id="twitterLogo" src="<?echo($config->get('baseUrl'))?>assets/img/twitterLogo.png">
            </a>

        </div>
    </div>

</div>
