<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <title>Serenatta</title>
    <link rel="stylesheet" type="text/css" href="<?echo($config->get('baseUrl'))?>assets/css/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="<?echo($config->get('baseUrl'))?>assets/css/style.css">
    <script src="<?echo($config->get('baseUrl'))?>assets/js/jquery.min.js"></script>
    <script src="<?echo($config->get('baseUrl'))?>assets/js/semantic.min.js"></script>

</head>
<body>

<?php $this->show($sSectionFile, get_defined_vars()['vars'])?>
<div id="footer">
    <?require_once('footer.php');?>
</div>
</div>
</body>
</html>