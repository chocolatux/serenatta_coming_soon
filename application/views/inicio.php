<div id="contenido" class="ui container">

    <div class="center aligned">
        <div class="centrarImg">
            <img class="ui image" src="<?echo($config->get('baseUrl'))?>assets/img/logo.png">
        </div>
        <p class="txtBlanco">La música en vivo va a ti</p>
    </div>

    <div id="divCaracteristicas">
        <p class="txtBlanco">Con <strong class="txtDorado">Serenatta</strong> podrás contratar músicos:</p>

        <div id="divItems" class="ui items">
            <div class="itemNuevo">
                <div class="ui mini image">
                    <img src="<?echo($config->get('baseUrl'))?>assets/img/cell.png">
                </div>
                <div class="middle aligned content">
                    <p class="txtDorado">Desde la comodidad de tu hogar, tablet o computadora</p>
                </div>
            </div>
            <div class="itemNuevo">
                <div class="ui mini image">
                    <img src="<?echo($config->get('baseUrl'))?>assets/img/clock.png">
                </div>
                <div class="middle aligned content">
                    <p class="txtDorado">De forma inmediata o para eventos posteriores</p>
                </div>
            </div>
            <div class="itemNuevo">
                <div class="ui mini image">
                    <img src="<?echo($config->get('baseUrl'))?>assets/img/position.png">
                </div>
                <div class="middle aligned content">
                    <p class="txtDorado">A tu ubicación actual o al lugar que los requieras</p>
                </div>
            </div>
        </div>
    </div>
    <div id="dividerSuperior" class="ui inverted divider"></div>

    <div id="divInput" class="center aligned">

            <p class="txtBlanco">Déjanos tu correo para avisarte cuando <strong class="txtDorado"> Serenatta </strong> esté listo</p>

            <form id="formEmail" class="ui form">
                <div class="field">
                    <input class="ui huge input" id="inputEmail" name="email" placeholder="Tu correo" type="text">
                    <button id="btnAvisenme" class="ui huge button btnSerenatta" >Avísenme</button>
                </div>

                <div class="ui error message"></div>
            </form>
    </div>

    <div id="mensajeConfirmacion" class="center aligned" style="display: none">
        <p class="txtBlanco">¡Muchas gracias por tu interés en <strong class="txtDorado">Serenatta</strong>!</p>

        <p class="txtBlanco">Muy pronto nos pondremos en contacto contigo para que seas parte de nuestro lanzamiento</p>
    </div>

    <div id="dividerInferior" class="ui inverted divider"></div>


    <div id="imgTiendas" class="center aligned">
        <p class="txtBlanco">Próximamente en:</p>
        <div id="imgAppStore" class="ui small image">
            <img  src="<?echo($config->get('baseUrl'))?>assets/img/appleStore.png">
        </div>
        <div class="ui small image">
            <img src="<?echo($config->get('baseUrl'))?>assets/img/playStore.png">
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){

        $('#formEmail').form({
            on: 'blur',
            fields: {
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type: 'email',
                            prompt: 'Error el email ingresado no es válido'
                        },
                        {
                            type: 'empty',
                            prompt: 'Ingresa un correo'
                        }
                    ]
                }
            },
            transition: 'fade down',
            onSuccess: function(event){
                   event.preventDefault();

                   var inputEmail = document.getElementById('inputEmail');

                   $.post('<?echo($config->get('baseUrl'))?>ajaxEnviar', {email: inputEmail.value}, function(data){

                       if(!data.success){
                           alert(data.message);
                           return;
                       }

                       document.getElementById('mensajeConfirmacion').style.display = "";
                       document.getElementById('divInput').style.display = "none";

                   }, 'json');

           }
        });
    });
</script>